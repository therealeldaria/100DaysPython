# Original Code
# year = input("Which year do you want to check?")
#
# if year % 4 == 0:
#     if year % 100 == 0:
#         if year % 400 == 0:
#             print("Leap year.")
#         else:
#             print("Not leap year.")
#     else:
#         print("Leap year.")
# else:
#     print("Not leap year.")

# Error:
# Traceback (most recent call last):
#   File "/home/brianl/PycharmProjects/100DaysPython/Day 13/Exercise2_Debugging_Leap_Year.py", line 4, in <module>
#     if year % 4 == 0:
# TypeError: not all arguments converted during string formatting

# Fixed Code:
year = int(input("Which year do you want to check?"))  # Input returns a string, should be an Int.

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print("Leap year.")
        else:
            print("Not leap year.")
    else:
        print("Leap year.")
else:
    print("Not leap year.")
