# Original Code
# number = int(input("Which number do you want to check?"))
#
# if number % 2 = 0:
#     print("This is an even number.")
# else:
#     print("This is an odd number.")

# Fixed code
number = int(input("Which number do you want to check?"))

if number % 2 == 0:  # Comparison needs double equal sigsn ==
    print("This is an even number.")
else:
    print("This is an odd number.")
