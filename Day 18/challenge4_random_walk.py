from turtle import Turtle, Screen
import random

colors = ["blue", "red", "purple", "green", "black", "yellow", "cyan", "grey", "pink"]

peppe = Turtle()
peppe.shape("circle")
peppe.shapesize(0.5, 0.5, 1)
peppe.resizemode("user")
peppe.pensize(10)
peppe.speed(0)


def turtle_move(direction):
    peppe.setheading(direction * 90)
    peppe.color(random.choice(colors))
    peppe.forward(20)


for _ in range(1000):
    turtle_move(random.randint(0, 3))

screen = Screen()
screen.exitonclick()
