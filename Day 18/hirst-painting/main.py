import turtle as t
import colorgram
import random

t.colormode(255)

c_colors = colorgram.extract('image.png', 30)

colors = []
for i in c_colors:
    r = i.rgb[0]
    g = i.rgb[1]
    b = i.rgb[2]
    colors.append((r, g, b))

peppe = t.Turtle()
peppe.hideturtle()
peppe.speed(0)
peppe.pu()

for v_pos in range(-360, 290, 70):
    for h_pos in range(-360, 290, 70):
        peppe.setpos(h_pos, v_pos)
        peppe.color(random.choice(colors))
        peppe.begin_fill()
        peppe.circle(20)
        peppe.end_fill()

screen = t.Screen()
screen.exitonclick()

