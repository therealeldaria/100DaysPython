from turtle import Turtle, Screen


peppe = Turtle()
peppe.shape("circle")
peppe.color("blue")

for i in range(40):
    if i % 2 == 0:
        peppe.pendown()
    else:
        peppe.penup()
    peppe.forward(10)

screen = Screen()
screen.exitonclick()