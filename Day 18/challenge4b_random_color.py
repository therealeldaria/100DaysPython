import turtle
from turtle import Turtle, Screen
import random


turtle.colormode(255)

peppe = Turtle()
peppe.shape("circle")
peppe.shapesize(0.5, 0.5, 1)
peppe.resizemode("user")
peppe.pensize(10)
peppe.speed(0)


def random_color():
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    return r, g, b


def turtle_move(direction):
    peppe.setheading(direction * 90)
    peppe.color(random_color())
    peppe.forward(20)


for _ in range(1000):
    turtle_move(random.randint(0, 3))

screen = Screen()
screen.exitonclick()
