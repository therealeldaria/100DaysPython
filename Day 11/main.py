from art import logo
from time import sleep

import random

cards = {
    "A": 11,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "10": 10,
    "J": 10,
    "Q": 10,
    "K": 10
}


# Game running check
def play_game():
    game_on = ""
    while game_on != "y" and game_on != "n":
        game_on = input("Do you want to play a game of Blackjack? Type 'y' or 'n': ").lower()
    if game_on == 'y':
        print(logo)
        return True
    else:
        return False


# Deal a card
def deal_card():
    card = random.choice(list(cards.keys()))
    return card


# Check if hand is JackPot
def check_jackpot(hand):
    if ("10" in hand or "J" in hand or "Q" in hand or "K" in hand) and "A" in hand:
        return True
    else:
        return False


# Format hand in a presentable way
def make_pretty(hand, dealer_first_hand):
    output = ""
    first_total = 0
    for item in range(0, len(hand)):
        if item == 0 and dealer_first_hand:
            output += "X, "
        else:
            if item == len(hand)-1:
                output += hand[item] + " | "
                first_total += cards[hand[item]]
            else:
                output += hand[item] + ", "
    if dealer_first_hand:
        total = first_total
    else:
        total = value_of_hand(hand)
    return output, total


# Display hand
def show_cards(player, dealer, dealer_first_hand):
    if player:
        player_pretty_hand, player_hand_total = make_pretty(hand=player, dealer_first_hand=False)
        print(f"Player hand: {player_pretty_hand}Value: {player_hand_total}")
    if dealer:
        dealer_pretty_hand, dealer_hand_total = make_pretty(hand=dealer, dealer_first_hand=dealer_first_hand)
        print(f"Dealer hand: {dealer_pretty_hand}Value: {dealer_hand_total}")


# Check value of hand
def value_of_hand(hand):
    value = 0
    for card in hand:
        value += cards[card]
    # Check if value is higher than 21, if so are there any Ace that can be changed from 11 to 1.
    ace = hand.count("A")
    if value > 21 and ace > 0:
        while ace > 0 and value > 21:
            value -= 10
            ace -= 1
    return value


game_running = play_game()

while game_running:
    player_hand = []
    dealer_hand = []
    # Deal initial cards
    for i in range(2):
        player_hand.append(deal_card())
        dealer_hand.append(deal_card())
    show_cards(player=player_hand, dealer=dealer_hand, dealer_first_hand=True)
    if check_jackpot(player_hand):
        print("Player has JackPot")
        show_cards(player=player_hand, dealer=dealer_hand, dealer_first_hand=False)
        if check_jackpot(dealer_hand):
            print("Dealer also has JackPot, it's a draw.")
        else:
            print("Player win!")
    else:
        dealing = True
        while dealing:
            player_hand_value = value_of_hand(player_hand)
            if player_hand_value < 22:
                player_choice = ""
                while player_choice != "hit" and player_choice != "stay":
                    player_choice = input("Do you want to hit or stand? Type 'hit' or 'stay': ").lower()
                if player_choice == "hit":
                    player_hand.append(deal_card())
                    show_cards(player=player_hand, dealer=False, dealer_first_hand=False)
                else:
                    dealing = False
            else:
                dealing = False
        if value_of_hand(player_hand) > 21:
            show_cards(player=player_hand, dealer=dealer_hand, dealer_first_hand=False)
            print("Player Bust!")
        else:
            show_cards(dealer=dealer_hand, player=False, dealer_first_hand=False)
            player_hand_value = value_of_hand(player_hand)
            dealer_hand_value = value_of_hand(dealer_hand)
            while dealer_hand_value < player_hand_value:
                print("Dealer takes another card.")
                dealer_hand.append(deal_card())
                dealer_hand_value = value_of_hand(dealer_hand)
                show_cards(player=False, dealer=dealer_hand, dealer_first_hand=False)
                sleep(1)
            show_cards(player=player_hand, dealer=dealer_hand, dealer_first_hand=False)
            if dealer_hand_value > 21:
                print("Dealer Bust, Player Wins!")
            else:
                print("Dealer Wins!")
    game_running = play_game()

print("Thank you for playing, good bye.")
