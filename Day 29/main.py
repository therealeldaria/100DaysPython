from tkinter import *
from tkinter import messagebox
from pyperclip import copy
from common import generate_password
import json


# ---------------------------- SAVE PASSWORD ------------------------------- #
def save_password():
    website = ent_website.get()
    username = ent_username.get()
    password = ent_password.get()

    missing = False
    err_text = ""
    entry_text = ""
    for key, entry in {"website": website, "username": username, "password": password}.items():
        if len(entry) < 1:
            missing = True
            err_text += f"{key}: Missing\n"
        else:
            entry_text += f"{key}: {entry}\n"

    if missing:
        err_text = f"Can't save when fields are missing.\n{err_text}"
        messagebox.showerror(title="Missing Entry", message=err_text)
    else:
        entry_text = f"Do you want to save the following details?\n{entry_text}"
        response = messagebox.askokcancel(title=website, message=entry_text)
        if response:
            save_entry(website, username, password)

            ent_website.delete(0, END)
            ent_password.delete(0, END)
            ent_website.focus()


def load_json():
    data = {}
    data_file = None
    try:
        data_file = open(file='data.json', mode='r')
    except FileNotFoundError:
        data_file = open(file='data.json', mode='w')
    else:
        data = json.load(data_file)
    finally:
        data_file.close()
    return data


def save_entry(website, username, password):
    save_data = {
        website: {
            "username": username,
            "password": password
        }
    }
    save_json(save_data)


def save_json(new_data):
    data = load_json()
    data.update(new_data)
    with open(file='data.json', mode='w') as data_file:
        json.dump(data, data_file, indent=4)


# ---------------------------- SEARCH --------------------------------- #
def search_entry():
    data = load_json()
    search_data = ent_website.get()
    if search_data:
        try:
            result = data[search_data]
        except KeyError:
            messagebox.showinfo(title="Not Found", message=f"Could not find {search_data}.\n"
                                                           f"Maybe it does not exists, of verify spelling.")
        else:
            username = result['username']
            password = result['password']
            copy(password)
            messagebox.showinfo(title=f"{search_data}", message=f"Website: {search_data}\n"
                                                                f"Username: {username}\n"
                                                                f"Password: {password}\n"
                                                                f"Password has been copied to Clipboard")
    else:
        messagebox.showerror(title="Missing entry", message="You need to write a Website to search for.")


# ---------------------------- UI SETUP ------------------------------- #

def new_password():
    password = generate_password()
    ent_password.delete(0, END)
    ent_password.insert(END, password)


window = Tk()
window.title("Password Manager")
window.config(pady=20, padx=20)

# Canvas
img_mypass = PhotoImage(file="logo.png")
canvas = Canvas(width=200, height=200)
canvas.create_image(120, 100, image=img_mypass)

# Labels
lbl_website = Label(text="Website:", justify="left", anchor="w")
lbl_username = Label(text="Email/Username:", justify="left", anchor="w")
lbl_password = Label(text="Password:", justify="left", anchor="w")

# Entries
ent_website = Entry(width=35)
ent_username = Entry(width=35)
ent_password = Entry(width=21)

# Buttons
but_generate = Button(text="Generate Password", command=new_password)
but_add = Button(text="Add", width=36, command=save_password)
but_search = Button(text="Search", command=search_entry)

# Setup Grid
canvas.grid(row=0, column=1)
lbl_website.grid(row=1, column=0, sticky=EW)
ent_website.grid(row=1, column=1, pady=3, sticky=EW)
but_search.grid(row=1, column=2, sticky=EW)
lbl_username.grid(row=2, column=0, sticky=EW)
ent_username.grid(row=2, column=1, pady=3, columnspan=2, sticky=EW)
lbl_password.grid(row=3, column=0, sticky=EW)
ent_password.grid(row=3, column=1, pady=3, sticky=EW)
but_generate.grid(row=3, column=2, sticky=EW)
but_add.grid(row=4, column=1, columnspan=2, sticky=NSEW)

# Update defaults
ent_website.focus()
ent_username.insert(END, "brian@eldaria.net")

window.mainloop()
