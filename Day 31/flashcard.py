from tkinter import *
import pandas


# Constants
BACKGROUND_COLOR = "#B1DDC6"
CARD_FILE = "data/french_words.csv"
CARD_DATA = pandas.DataFrame()
COMPLETED_DATA = pandas.DataFrame()
CURRENT_CARD = pandas.DataFrame()


# File Handling
def load_data():
    global CARD_DATA, COMPLETED_DATA
    CARD_DATA = pandas.read_csv(CARD_FILE)
    try:
        COMPLETED_DATA = pandas.read_csv("autosave.csv")
    except FileNotFoundError:
        COMPLETED_DATA = pandas.DataFrame(columns=CARD_DATA.columns)


def autosave():
    COMPLETED_DATA.to_csv("autosave.csv", index=False)


def show_next_card():
    global CURRENT_CARD
    merged_data = CARD_DATA.merge(COMPLETED_DATA, how='left', indicator=True)
    cards_available = merged_data[merged_data['_merge'] == 'left_only']
    cards_available = cards_available.drop(columns='_merge')
    if len(cards_available) < 1:
        cards_completed()
    else:
        CURRENT_CARD = cards_available.sample(n=1)
        column_names = CARD_DATA.columns.tolist()
        canvas.itemconfig(card, image=img_card_front)
        canvas.itemconfig(flash_language_front, text=column_names[0])
        canvas.itemconfig(flash_word_front, text=CURRENT_CARD.iloc[0, 0])
        but_correct.config(state=DISABLED)
        but_wrong.config(state=DISABLED)
        window.after(3000, show_answer)


def show_answer():
    but_correct.config(state=NORMAL)
    but_wrong.config(state=NORMAL)
    canvas.itemconfig(card, image=img_card_back)
    column_names = CARD_DATA.columns.tolist()
    canvas.itemconfig(flash_language_front, text=column_names[1])
    canvas.itemconfig(flash_word_front, text=CURRENT_CARD.iloc[0, 1])


def start_game():
    global CARD_DATA, COMPLETED_DATA, CURRENT_CARD
    but_correct.config(command=correct_answer)
    but_wrong.config(command=wrong_answer)
    # Set up the source and completed data_frames
    canvas.itemconfig(card, image=img_card_front)
    load_data()
    show_next_card()


def restart_game():
    global COMPLETED_DATA
    COMPLETED_DATA = COMPLETED_DATA.drop(index=COMPLETED_DATA.index)
    autosave()
    start_game()


def cards_completed():
    canvas.itemconfig(card, image=img_card_front)
    canvas.itemconfig(flash_language_front, text="Cards Completed.")
    canvas.itemconfig(flash_word_front, text="Restart?")
    but_correct.config(command=restart_game)
    but_wrong.config(command=quit_game)


def quit_game():
    window.quit()


def correct_answer():
    global COMPLETED_DATA
    COMPLETED_DATA = pandas.concat([COMPLETED_DATA, CURRENT_CARD], ignore_index=True)
    autosave()
    show_next_card()


def wrong_answer():
    show_next_card()


# GUI Setup

# screen
window = Tk()
window.title("Flashy")
window.config(pady=50, padx=50, bg=BACKGROUND_COLOR)

# Load some images to be used
img_card_back = PhotoImage(file='images/card_back.png')
img_card_front = PhotoImage(file='images/card_front.png')
img_correct = PhotoImage(file='images/right.png')
img_wrong = PhotoImage(file='images/wrong.png')

# Canvas
canvas = Canvas(width=800, height=526, highlightthickness=0, bg=BACKGROUND_COLOR)
card = canvas.create_image(400, 263, image=img_card_front)
flash_language_front = canvas.create_text(400, 150, text="Flash Card Game", font=('Ariel', 40, 'italic'))
flash_word_front = canvas.create_text(400, 263, text="Start Game?", font=('Ariel', 60, 'bold'))

# Buttons
but_correct = Button(
    image=img_correct,
    highlightthickness=0,
    bg=BACKGROUND_COLOR,
    borderwidth=0,
    activebackground=BACKGROUND_COLOR,
    activeforeground=BACKGROUND_COLOR,
    command=start_game,
)
but_wrong = Button(
    image=img_wrong,
    highlightthickness=0,
    bg=BACKGROUND_COLOR,
    borderwidth=0,
    activebackground=BACKGROUND_COLOR,
    activeforeground=BACKGROUND_COLOR,
    command=quit_game,
)

# Grid Layout
canvas.grid(row=0, column=0, columnspan=2)
but_wrong.grid(row=1, column=0)
but_correct.grid(row=1, column=1)

# app loop
window.mainloop()
