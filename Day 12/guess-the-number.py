import random

number_to_guess = random.randint(1, 100)
print("Welcome to the Number Guessing Game!\n"
      "I'm thinking of a number between 1 and 100.")
level = ""
while level != "easy" and level != "hard":
    level = input("Choose a difficulty. Type 'easy' or 'hard': ").lower()
if level == "easy":
    level = 10
else:
    level = 5

for guesses in range(level, 0, -1):
    print(f"You have {guesses} attempts remaning to guess the number.")
    guess = int(input("Make a guess: "))
    if guess > number_to_guess:
        print("Too high.")
    elif guess < number_to_guess:
        print("Too low.")
    else:
        print("You guessed the number, congratulations!")
        break
    if guesses > 1:
        print("Guess again.")
    else:
        print("You did not guess the number, Game Over.")
