from turtle import Screen, Turtle
from PIL import Image
import tempfile
import pandas


SPEED = 100
SCREEN_SIZE = (1087, 736)
BACKGROUND_IMAGE = "blank_states_img.gif"


def set_bg_image(screen):
    with tempfile.NamedTemporaryFile(delete=True, suffix=".gif") as temp:
        bg_image = Image.open(BACKGROUND_IMAGE)
        resized_image = bg_image.resize(SCREEN_SIZE)
        resized_image.save(temp.name, "GIF")
        screen.bgpic(temp.name)


class Game:
    def __init__(self):
        self.screen = Screen()
        self.speed = SPEED
        self.game_running = True
        self.screen_size = SCREEN_SIZE
        self.screen.setup(width=self.screen_size[0], height=self.screen_size[1])
        self.screen.getcanvas().winfo_toplevel().resizable(False, False)
        set_bg_image(self.screen)
        self.cursor_turtle = Turtle(visible=False)
        self.cursor_turtle.penup()
        self.cursor_turtle.speed('fastest')
        self.states = pandas.read_csv("50_states.csv")

    def write_state(self, state, x, y):
        self.cursor_turtle.goto(x, y)
        self.cursor_turtle.write(f"{state}", align="center", font=("Arial", 12, "normal"))

    def ask_state(self):
        user_input = self.screen.textinput("State", "Please enter the name of a U.S State.")
        if user_input:
            state = user_input.capitalize()
            if state in self.states.state.values:
                x = self.states.x[self.states.state == state].values[0]
                y = self.states.y[self.states.state == state].values[0]
                self.write_state(state, x, y)
            self.ask_state()


if __name__ == '__main__':
    game = Game()
    game.screen.ontimer(game.ask_state, 100)
    game.screen.mainloop()
