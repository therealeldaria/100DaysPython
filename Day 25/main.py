import pandas

# data = pandas.read_csv("weather_data.csv")
# print(type(data))
# print(type(data["temp"]))
#
# data_dict = data.to_dict()
# print(data_dict)
#
# temp_list = data["temp"].to_list()
# print(len(temp_list))
#
# print(data["temp"].mean())
# print(data["temp"].max())
#
# # Get Data in Columns
# print(data["condition"])
# print(data.condition)

# Get Data in Row
# print(data[data.day == "Monday"])
# print(data[data.temp == data.temp.max()])


def convert_to_fahrenheit(c):
    f = (c * 9 / 5) + 32
    return f

# monday = data[data.day == "Monday"]
# print(convert_to_fahrenheit(monday.temp.item()))


squirrel_data = pandas.read_csv("2018_squirrel_count_sensus.csv")
squirrel_data["Primary Fur Color"].value_counts().to_csv("squirrel_count.csv")

