from turtle import Turtle
STARTING_POSITION = (0, -280)
MOVE_DISTANCE = 10
FINISH_LINE_Y = 280


# The Player Class
class Player:
    def __init__(self):
        self.position = STARTING_POSITION
        self.alive = True
        self.level_up = False
        self._create_player()

        # Function to create the player
    def _create_player(self):
        self.player = Turtle()
        self.player.penup()
        self.player.setposition(self.position)
        self.player.shape("turtle")
        self.player.color("black")
        self.player.setheading(90)

    # Move the player
    def move(self):
        self.player.forward(MOVE_DISTANCE)
        self.position = self.player.position()
        if self.position[1] >= FINISH_LINE_Y:
            self.player.setposition(STARTING_POSITION)
            self.level_up_player()

    def kill(self):
        self.alive = False

    def level_up_player(self):
        self.level_up = True

    def is_level_up(self):
        level_up = self.level_up
        self.level_up = False
        return level_up
