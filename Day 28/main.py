from tkinter import *


# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
TIMER_RUNNING = FALSE
APP_STATE = "IDLE"
APP_STATES = {
    "IDLE": {
        "next": "WORK",
        "handler": lambda: None,
    },
    "WORK": {
        "rep": 0,
        "next": "SHORT_BREAK",
        "handler": lambda: None,
    },
    "SHORT_BREAK": {
        "next": "WORK",
        "handler": lambda: None,
    },
    "LONG_BREAK": {
        "next": "IDLE",
        "handler": lambda: None,
    }
}


def idle_handler():
    global TIMER_RUNNING
    if TIMER_RUNNING:
        return
    lbl_txt_timer.config(text="Timer")


def work_handler():
    global TIMER_RUNNING
    if TIMER_RUNNING:
        return
    lbl_txt_timer.config(text="Work")
    start_timer(WORK_MIN)
    TIMER_RUNNING = True


def short_break_handler():
    global TIMER_RUNNING
    if TIMER_RUNNING:
        return
    lbl_txt_timer.config(text="Short Break")
    start_timer(SHORT_BREAK_MIN)
    TIMER_RUNNING = True


def long_break_handler():
    global TIMER_RUNNING
    if TIMER_RUNNING:
        return
    lbl_txt_timer.config(text="LONG BREAK")
    start_timer(LONG_BREAK_MIN)
    TIMER_RUNNING = True
    APP_STATES["WORK"]["rep"] = 0


def update_state():
    global APP_STATE
    if APP_STATE == "WORK":
        APP_STATES["WORK"]["rep"] += 1
        if APP_STATES["WORK"]["rep"] > 3:
            APP_STATE = "LONG_BREAK"
        else:
            APP_STATE = APP_STATES[APP_STATE]["next"]
    else:
        APP_STATE = APP_STATES[APP_STATE]["next"]


def main_loop():
    handler = APP_STATES[APP_STATE]["handler"]
    handler()

    window.after(1000, main_loop)


# ---------------------------- TIMER RESET ------------------------------- # 

# ---------------------------- TIMER MECHANISM ------------------------------- #
def start_timer(duration):
    count_down(duration)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #
def count_down(count):
    global TIMER_RUNNING
    canvas.itemconfig(timer_text, text=convert_seconds_to_clock(count))
    if count > 0:
        window.after(1000, count_down, count - 1)
    elif count == 0:
        TIMER_RUNNING = FALSE
        update_state()


def convert_seconds_to_clock(seconds):
    minutes, seconds = divmod(seconds, 60)
    return "{:02d}:{:02d}".format(minutes, seconds)


# ---------------------------- SETUP ---------------------------------- #
APP_STATES["IDLE"]["handler"] = idle_handler
APP_STATES["WORK"]["handler"] = work_handler
APP_STATES["SHORT_BREAK"]["handler"] = short_break_handler
APP_STATES["LONG_BREAK"]["handler"] = long_break_handler


# ---------------------------- UI SETUP ------------------------------- #
# Setup Window
window = Tk()
window.title("Pomodore")
window.config(padx=100, pady=50, bg=YELLOW)


# Add canvas
canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)
tomato_img = PhotoImage(file="tomato.png")
canvas.create_image(100, 112, image=tomato_img)
timer_text = canvas.create_text(100, 130, text="00:00", fill="white", font=(FONT_NAME, 25, "bold"))

# Add Labels
lbl_txt_timer = Label(text="Timer", font=(FONT_NAME, 45, "bold"), fg=GREEN, bg=YELLOW, highlightthickness=0)
lbl_txt_poms = Label(text="✔", font=(FONT_NAME, 15), fg=GREEN, bg=YELLOW, highlightthickness=0)

# Add Buttons
but_start = Button(text="Start", font=(FONT_NAME, 10), fg=PINK, highlightthickness=0,
                   command=lambda: update_state() if APP_STATE == "IDLE" else None)
but_reset = Button(text="Reset", font=(FONT_NAME, 10), fg=PINK, highlightthickness=0)

# Setup Grid
lbl_txt_timer.grid(row=0, column=0, columnspan=3)
canvas.grid( row=1, column=1)
but_start.grid(row=2, column=0)
but_reset.grid(row=2, column=2)
lbl_txt_poms.grid(row=3, column=1)

main_loop()
# Run mainloop
window.mainloop()
