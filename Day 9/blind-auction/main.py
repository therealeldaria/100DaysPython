from replit import clear
from art import logo


def print_art():
    print(logo)


def add_bid():
    get_name = input("Please enter your name: ")
    get_bid = int(input("Please enter your bid: "))
    return get_name, get_bid


def print_winning_bid(the_bids):
    highest_bid = 0
    bidder = ""
    for bid_name in the_bids:
        if the_bids[bid_name] > highest_bid:
            highest_bid = the_bids[bid_name]
            bidder = bid_name
    print(f"The highest bid {highest_bid}, was cast by {bidder}\n")


app_running = True
bids = {}
while app_running:
    print_art()
    name, bid = add_bid()
    bids[name] = bid
    new_bidder = input("Press enter for a new bidder \n"
                       "or type 'q' and then press enter to to see winning bid and end program: \n").lower()
    if new_bidder == "q":
        print_winning_bid(bids)
        app_running = False
