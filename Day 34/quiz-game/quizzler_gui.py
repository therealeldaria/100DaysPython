from quiz_brain import QuizBrain
from question_loader import QuestionLoader
from ui import QuizGUI


qLoader = QuestionLoader()
question_bank = qLoader.load_questions(question_amount=10, difficulty=1)

qBrain = QuizBrain(question_bank)

qui = QuizGUI(qBrain)

