from question_model import Question


class QuizBrain:
    def __init__(self, question_list):
        self.question = Question
        self.question_number = 0
        self.question_list = question_list
        self.score = 0

    def next_question(self):
        self.question = self.question_list[self.question_number]
        self.question_number += 1
        return f"Q.{self.question_number}: {self.question.text}"

    def still_has_questions(self):
        return self.question_number < len(self.question_list)

    def check_answer(self, answer):
        if answer == self.question.answer:
            self.score += 1
            return True
        return False



