from turtle import Turtle

DEFAULT_SPEED = 3


class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.color("white")
        self.speed("fastest")
        self.penup()
        self.ball_speed = DEFAULT_SPEED
        self.y_direction_up = False
        self.x_direction_right = True
        self.draw_midline()

    def draw_midline(self):
        self.setposition(x=0, y=300)
        self.setheading(270)
        self.shape("square")
        self.pensize(10)
        draw = True
        for i in range(300, -300, -25):
            if draw:
                self.pendown()
            else:
                self.penup()
            self.forward(25)
            draw = not draw
        self.penup()
        self.shape("circle")
        self.setheading(90)
        self.setposition(x=0, y=0)

    def move(self):
        if self.y_direction_up:
            new_y = self.ycor() + self.ball_speed
        else:
            new_y = self.ycor() - self.ball_speed
        if self.x_direction_right:
            new_x = self.xcor() + self.ball_speed
        else:
            new_x = self.xcor() - self.ball_speed
        self.setposition(x=new_x, y=new_y)

    def collide(self, collision_type):
        if collision_type == "edge":
            self.y_direction_up = not self.y_direction_up
        elif collision_type == "paddle":
            self.x_direction_right = not self.x_direction_right

    def accelerate(self):
        self.ball_speed += 1

    def reset_ball(self):
        self.setposition(0, 0)
        self.ball_speed = DEFAULT_SPEED