from art import logo

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


def encrypt_decrypt_letter(input_letter, shift_number, operation):
    position = alphabet.index(input_letter)
    if operation == "encode":
        return alphabet[(position + shift_number) % len(alphabet)]
    else:
        return alphabet[(position - shift_number) % len(alphabet)]


program_running = True
while program_running:
    print(logo)
    direction = ""
    text = ""
    shift = 0
    while not (direction == "encode" or direction == "decode"):
        direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")

    while len(text) < 1:
        text = input("Type your message:\n").lower()

    while shift == 0:
        shift = int(input("Type the shift number:\n"))
    out_text = ""

    for letter in text:
        if letter in alphabet:
            out_text += encrypt_decrypt_letter(input_letter=letter, shift_number=shift, operation=direction)
        else:
            out_text += letter

    print(f"Here's the {direction}d result: {out_text}")
    if input("Type 'yes' if you want to continue, Otherwise type 'no':\n") != "yes":
        program_running = False
