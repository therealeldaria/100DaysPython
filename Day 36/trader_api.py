from config import Config
from typing import Union, Any
import requests
from datetime import datetime, timedelta


# Some Global Variables
STOCK = "TSLA"
COMPANY_NAME = "Tesla Inc"

# Load Configurations
api_config = Config("../api_config.json", True)
api_config.load()


# STEP 1: Use https://www.alphavantage.co
# When STOCK price increase/decreases by 5% between yesterday and the day before yesterday then print("Get News").
def five_percent_change(symbol: str) -> Union[bool, tuple[str, float]]:
    """
    Check if there is a more than 5% change to the stock price last two close.

    :param symbol: The stock symbol.
    :type symbol: str
    :return: False if there isn't a more than 5% change, otherwise a tuple with date of the change and the percentage.
    :rtype: Union[bool, tuple[str, float]]
    """
    av_api_url = api_config.get("url", "apis", "alphavantage")
    av_api_key = api_config.get("key", "apis", "alphavantage")
    parameters = {
        "function": "TIME_SERIES_DAILY",
        "symbol": symbol,
        "outputsize": "compact",
        "datatype": "json",
        "apikey": av_api_key
    }
    response = requests.get(url=av_api_url, params=parameters)
    response.raise_for_status()
    data = response.json()
    last_two_dates = sorted(data['Time Series (Daily)'].keys(), reverse=True)[:2]
    day_values = [(date, float(data['Time Series (Daily)'][date]['4. close'])) for date in last_two_dates]
    percentage_diff = ((day_values[0][1] - day_values[1][1]) / day_values[1][1]) * 100

    if abs(percentage_diff) > 5:
        return day_values[1][0], percentage_diff
    return False


# STEP 2: Use https://newsapi.org
# Instead of printing ("Get News"), actually get the first 3 news pieces for the COMPANY_NAME. 
def get_news(company: str, date: str) -> list[dict[str: str]]:
    """
    Get the top 3 news headlines and description for provided company name.

    :param company: The company name.
    :type company: str
    :param date: The date you want news for
    :type date: str
    :return: List with dictionary with headlines and description
    :rtype: list[dict[str: str]]
    """
    news_api_url = api_config.get("url", "apis", "newsapi")
    news_api_key = api_config.get("key", "apis", "newsapi")
    from_date = datetime.strptime(date, '%Y-%m-%d')
    a_day_later = from_date + timedelta(days=1)
    to_date = a_day_later.strftime('%Y-%m-%d')

    parameters = {
        "q": company,
        "from": date,
        "to": to_date,
        "language": "en",
        "sortBy": "popularity",
        "apiKey": news_api_key
    }
    response = requests.get(url=news_api_url, params=parameters)
    response.raise_for_status()
    data = response.json()['articles'][:3]
    filtered = [{'title': item['title'], 'description': item['description']} for item in data]
    return filtered


# STEP 3: Use Telegram
# Send a separate message with the percentage change and each article's title and description to your telegram.
def send_tg_message(message: str) -> Any:
    """
    Send message using Telegram API.

    :param message: The message to send.
    :type message: str
    :return: The response from API
    :rtype: Any
    """
    tg_api_url = api_config.get("url", "apis", "telegram")
    tg_api_chat_id = api_config.get("chat_id", "apis", "telegram")
    tg_api_key = api_config.get("key", "apis", "telegram")

    url = f"{tg_api_url}{tg_api_key}/sendMessage"

    parameters = {
        "chat_id": tg_api_chat_id,
        "text": message
    }

    response = requests.post(url, data=parameters)
    return response.json()


should_we_send = five_percent_change(symbol=STOCK)
if should_we_send:
    message = f"{STOCK}: "
    if should_we_send[1] > 0:
        message += f"🔺 "
    else:
        message += f"🔻 "
    message += f"{should_we_send[1]:.2f}%\n"
    news = get_news(company=COMPANY_NAME, date=should_we_send[0])
    for article in news:
        send_message = message
        send_message += f"Headline: {article['title']}\n"
        send_message += f"Brief: {article['description']}"
        send_tg_message(message=send_message)


# Optional: Format the SMS message like this:
"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""

