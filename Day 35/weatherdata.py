from config import Config
import requests


local_config = Config("config.json", True)
local_config.load()

MY_LAT_LONG = (local_config.get("latitude"), local_config.get("longitude"))

api_config = Config("../api_config.json", True)
api_config.load()

# Get OpenWeather details from config
ow_api_url = api_config.get("url", "apis", "openweathermap")
ow_api_key = api_config.get("key", "apis", "openweathermap")

# Get Telegram details from config
tg_api_url = api_config.get("url", "apis", "telegram")
tg_api_bot = api_config.get("bot_id", "apis", "telegram")
tg_api_chat_id = api_config.get("chat_id", "apis", "telegram")
tg_api_key = api_config.get("key", "apis", "telegram")


def rain_expected():
    parameters = {
        "lat": MY_LAT_LONG[0],
        "lon": MY_LAT_LONG[1],
        "appid": ow_api_key,
        "exclude": "current,minutely,daily,alerts",
        "units": "metric"
    }

    response = requests.get(url=ow_api_url, params=parameters)
    response.raise_for_status()
    data = response.json()
    weather = [hour['weather'][0]['id'] for hour in data['hourly'][:12]]
    is_rain = any(weather_id < 700 for weather_id in weather)
    return is_rain


def send_tg_message(message):
    url = f"{tg_api_url}{tg_api_key}/sendMessage"

    parameters = {
        "chat_id": tg_api_chat_id,
        "text": message
    }

    response = requests.post(url, data=parameters)
    return response.json()


if rain_expected():
    send_tg_message("It is going to rain today, Bring an umbrella.")
