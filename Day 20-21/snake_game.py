from turtle import Screen
from snake import Snake
from food import Food
from scoreboard import Score

SPEED = 120
screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("black")
screen.title(titlestring="My Snake Game")
screen.tracer(0)

snake = Snake()
food = Food()
score = Score()
game_running = True


def game_loop():
    global game_running
    if game_running:
        snake.move()
        if snake.head.distance(food) < 10:
            food.update_pos()
            snake.add_segment()
            score.add_point()
        head_pos = snake.head.position()
        if head_pos[0] > 280 or head_pos[0] < -280 or head_pos[1] > 280 or head_pos[1] < -280\
                or head_pos in snake.snake_body:
            snake.reset()
            score.game_over()
        screen.update()
        screen.ontimer(game_loop, SPEED)
    else:
        print("end")


screen.listen()
screen.onkey(snake.right, 'Right')
screen.onkey(snake.left, 'Left')
screen.onkey(snake.up, 'Up')
screen.onkey(snake.down, 'Down')

game_loop()

screen.mainloop()