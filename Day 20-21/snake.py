from turtle import Turtle

START_POS = [(0, 0), (-20, 0), (-40, 0)]
MOVE_DISTANCE = 20
UP = 90
LEFT = 180
DOWN = 270
RIGHT = 0


class Snake:
    def __init__(self):
        self.segments = []
        self.snake_body = []
        self.__create_snake()
        self.head = self.segments[0]
        self.has_turned = False

    def __create_snake(self):
        for position in START_POS:
            self.create_segment(position)
        self.segments[0].color("red")

    def create_segment(self, position):
        new_segment = Turtle(shape="square")
        new_segment.penup()
        new_segment.color("white")
        new_segment.setposition(position)
        self.segments.append(new_segment)

    def reset(self):
        segments = self.segments
        for segment in segments:
            segment.clear()
            segment.hideturtle()
            del segment
        self.segments.clear()
        self.__create_snake()
        self.head = self.segments[0]

    def add_segment(self):
        self.create_segment(self.segments[-1].position())

    def move(self):
        self.snake_body.clear()
        for seg_num in range(len(self.segments) - 1, 0, -1):
            new_pos = self.segments[seg_num - 1].position()
            self.segments[seg_num].setposition(new_pos)
            self.snake_body.append(new_pos)
        self.head.forward(MOVE_DISTANCE)
        self.has_turned = False

    def left(self):
        if self.head.heading() != RIGHT and not self.has_turned:
            self.head.setheading(LEFT)
            self.has_turned = True

    def right(self):
        if self.head.heading() != LEFT and not self.has_turned:
            self.head.setheading(RIGHT)
            self.has_turned = True

    def up(self):
        if self.head.heading() != DOWN and not self.has_turned:
            self.head.setheading(UP)
            self.has_turned = True

    def down(self):
        if self.head.heading() != UP and not self.has_turned:
            self.head.setheading(DOWN)
            self.has_turned = True
