from turtle import Turtle
ALIGNMENT = "center"
FONT = ("Comic Sans", 18, "normal")
SCOREBOARD_POSITION = (0, 270)


class Score(Turtle):
    def __init__(self):
        super().__init__()
        self.current_score = 0
        self.high_score = 0
        self.penup()
        self.hideturtle()
        self.setposition(SCOREBOARD_POSITION)
        self.color("white")
        self.speed("fastest")
        self.load_high_score()
        self.update_score()

    def update_score(self):
        self.clear()
        self.write(
            f"Score: {self.current_score}  High Score: {self.high_score}",
            move=False,
            align=ALIGNMENT,
            font=FONT)

    def load_high_score(self):
        try:
            with open("data.txt", "r") as file:
                self.high_score = int(file.read())
        except FileNotFoundError:
            with open("data.txt", "w") as file:
                file.write("0")
            self.high_score = 0

    def save_high_score(self):
        with open("data.txt", "w") as file:
            file.write(str(self.high_score))

    def add_point(self):
        self.current_score += 1
        self.update_score()

    def game_over(self):
        self.high_score = max(self.high_score, self.current_score)
        self.save_high_score()
        self.current_score = 0
        self.update_score()
