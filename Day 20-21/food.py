from turtle import Turtle
from random import randint


class Food(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.penup()
        self.shapesize(stretch_len=0.5, stretch_wid=0.5)
        self.color("orange")
        self.speed("fastest")
        self.update_pos()

    def update_pos(self):
        rand_x = randint(-14, 14) * 20
        rand_y = randint(-14, 13) * 20
        self.setposition(x=rand_x, y=rand_y)
