from random import choice, randint, shuffle
from string import ascii_letters, printable


# ---------------------------- PASSWORD GENERATOR ------------------------------- #
def generate_password(length: int = 16,
                      letters: bool = True,
                      letters_weight: int = 8,
                      symbols: bool = True,
                      symbols_weight: int = 3,
                      numbers: bool = True,
                      numbers_weight: int = 3
                      ) -> str:
    """
    Create a random password.

    :param length: The final length of the password. Default is 16.
    :type length: int
    :param letters: Should password have letters? Default is True.
    :type letters: bool
    :param letters_weight: What weight should letters have in the final password.
                           Default is 8. Acceptable range is [1, 10].
    :type letters_weight: int
    :param symbols: Should password have symbols? Default is True.
    :type symbols: bool
    :param symbols_weight: What weight should symbols have in the final password
                           Default is 3. Acceptable range is [1, 10].
    :type symbols_weight: int
    :param numbers: Should password have numbers? Default is True.
    :type numbers: bool
    :param numbers_weight: What weight should numbers have in the final password
                           Default is 3. Acceptable range is [1, 10].
    :type numbers_weight: int
    :return: String with a random password
    :rtype: str
    """
    def adjust_weight(weight: int) -> int:
        return max(1, min(weight, 10))

    letters_weight = adjust_weight(letters_weight)
    symbols_weight = adjust_weight(symbols_weight)
    numbers_weight = adjust_weight(numbers_weight)

    symbols_list = []
    for char in printable:
        if not char.isalnum() and not char.isspace() and char not in ("'", '"', '|'):
            symbols_list.append(char)

    total_weight = (letters * letters_weight) + (symbols * symbols_weight) + (numbers * numbers_weight)

    letters_count = (letters * letters_weight * length) // total_weight
    symbols_count = (symbols * symbols_weight * length) // total_weight
    numbers_count = length - letters_count - symbols_count

    password_list = []
    if letters:
        password_list.extend([choice(ascii_letters) for _ in range(letters_count)])
    if symbols:
        password_list.extend([choice(symbols_list) for _ in range(symbols_count)])
    if numbers:
        password_list.extend([str(randint(0, 9)) for _ in range(numbers_count)])

    shuffle(password_list)

    password = choice(ascii_letters) + "".join(password_list)[:length-1]

    return password
